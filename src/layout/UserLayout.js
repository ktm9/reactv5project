import { Breadcrumb, Layout, theme } from 'antd';
import { Content, Footer, Header } from 'antd/es/layout/layout';
import React from 'react'
import { Outlet } from 'react-router-dom'
import MainHeaders from "./header"


function UserLayout() {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  return (
    <Layout>
      <Header
        style={{
          color:"white"
        }}
      >
     
        <MainHeaders/>
       

      </Header>
      <Content
        style={{
          padding: "0 48px",
        }}
      >
        {/* <Breadcrumb
          style={{
            margin: "16px 0",
          }}
        >
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>List</Breadcrumb.Item>
          <Breadcrumb.Item>App</Breadcrumb.Item>
        </Breadcrumb> */}
        <Layout
          style={{
            padding: "24px 0",
            background: colorBgContainer,
            borderRadius: borderRadiusLG,
          }}
        >
         
          <Content
            style={{
              padding: "0 24px",
              minHeight: 280,
            }}
          >
            <Outlet />
          </Content>
        </Layout>
      </Content>
      <Footer
        style={{
          textAlign: "center",
          
        }}
      >
        ©{new Date().getFullYear()} Created by Chyanti Company
      </Footer>
    </Layout>
  );
}

export default UserLayout
