import { Button, Checkbox, Form, Input } from "antd";

const formSubmit = () => {
  const onFinish = (values) => {
    console.log("values", values);
  };
  
  // const formLayout={
  //   labelcol:{span:24},
  //   wrappercol:{span:24}
  // }
  return (
    <div>
      <Form onFinish={onFinish} layout="vertical">
        <Form.Item
          name={"first_name"}
          label="First Name"
          rules={[{ required: true, message: "Please enter first name !" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name={"last_name"}
          label="Last Name"
          rules={[{ required: true, message: "Please enter last name !" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item name={"activate"}  valuePropName="checked">
          <Checkbox >active</Checkbox>
        </Form.Item>

        <Form.Item name={"textarea"} label="description">
          <Input.TextArea />
        </Form.Item>

        <Form.Item name={"password"} label="password">
          <Input.Password />
        </Form.Item>

        <Form.Item
          name={"email"}
          label="email"
          rules={[
            {
              type: "email",
              required: true,
              message: "Please enter valid email",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
  name={"phoneNumber"}
  label="Phone Number"
  rules={[
    {
      pattern: /^[0-9]{10}$/, // Regex pattern for 10-digit number
      message: "Please enter a valid 10-digit phone number",
    },
    {
      required: true,
      message: "Please enter your phone number",
    },
  ]}
>
  <Input />
</Form.Item>

   

        <Button htmlType="submit">Form Submit</Button>
      </Form>
    </div>
  );
};
export default formSubmit;

//label lekheko chai browser ma label dekhauna
//form .item chai form ko  types jastai text area,password haru rakhna
//rules rakheko chai validation ko lagi
//
