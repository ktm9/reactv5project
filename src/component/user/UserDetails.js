import { EyeOutlined } from "@ant-design/icons";
import { Rate } from "antd";
import Card from "antd/es/card/Card";
import React from "react";
import { useAppContext } from "../../ContextApi";

const UserDetails = () => {
  const{appState}=useAppContext();
  // const myValue = localStorage.getItem("userdetail");
  // const data = JSON.parse(myValue);
  // console.log("userdetail", JSON.parse(myValue));
  console.log("userdetail", appState);
  return (
    <div style={{ display: "flex", justifyContent: "flex-start", gap: "4px" }}>
      <div>
        userdetail
        <Card
          hoverable
          style={{ width: 240 }}
          cover={<img alt="example" src={appState?.detail?.image} />}
        ></Card>
      </div>
      <div>
        <div>{appState?.detail.name}</div>
        <div>
          Rate:
          <Rate value={appState?.detail?.rate} />
        </div>
        <div>
          
          <EyeOutlined/>view:{appState?.detail?.view}
        </div>
      </div>
      <div></div>
    </div>
  );
};

export default UserDetails;
