import React from "react";
import { Button, Card } from "antd";
import { json, useNavigate } from "react-router-dom";
import { useAppContext } from "../../ContextApi";

const UserItems = ({ data, title }) => {
  
  console.log("data", data, title);
  const {appState,updateState}=useAppContext()
  const navigate=useNavigate()
  const handleClick=(item)=>{
    updateState({
      ...appState,
      detail:item
    })

    navigate(`/userdetail/:${item.id}`)
  }
    
   
    // localStorage.setItem('userdetail',JSON.stringify(item))
    
  
  const addToCart=()=>{
    console.log(appState)

  }

  return (
    <div>
      <div>{title}</div>
      <div
        style={{ display: "flex", justifyContent: "flex-start", gap: "4px" }}
      >
        {data?.map((item) => (
          <div key={item.id} onClick={()=>handleClick(item)}>
            <Card
              hoverable
              style={{ width: 240 }}
              cover={<img alt="example" src={item.image} />}
            >
              <div>price: {item.price}</div>
              <div>brand: {item.brand}</div>
              <div>
                <Button onClick={addToCart} >add to card</Button>
              </div>
              

            </Card>
          </div>
        ))}
      </div>
    </div>
  );
};

export default UserItems;
