import React from 'react'
import UserItems from './UserItems'
import { HotProductData } from '../../utils/Items'

const Hotproducts = () => {
  return (
    <div>
      <UserItems data={HotProductData} title="hot products"/>
    </div>
  )
}

export default Hotproducts
