import React from 'react'
import { Button, Checkbox, Form, Input } from 'antd';
import Card from 'antd/es/card/Card'
const SignUp = () => {
  return (
    <div>
       <div style={{width:"300px",height:"auto",margin:"auto",justifyContent:"center"}}>
        <Card>
        <Form layout='vertical'
  
  >
    <Form.Item
      label="first name"
      name={'first name'}
     
    >
      <Input />
    </Form.Item>

    <Form.Item
      label="last name"
      name={'last name'}
     
    >
      <Input />
    </Form.Item>
    <Form.Item
      label="contact"
      name={'contact'}
     
    >
      <Input />
    </Form.Item>


    <Form.Item
      name={['user', 'email']}
      label="Email"
      rules={[
        {
          type: 'email',
        },
      ]}
    >
      <Input />
    </Form.Item>

    
    

    <Form.Item
      label="Password"
      name={"password"}
      
    >
      <Input.Password />
    </Form.Item>

    

    <Form.Item
      label="confirm Password"
      name={"confirm password"}
      
    >
      <Input.Password />
    </Form.Item>

    
    <Form.Item
    
   
    >
      <Button >
        Login
      </Button>
    </Form.Item>
  </Form>
        </Card>
      
    </div>
  )
    </div>
  )
}

export default SignUp
