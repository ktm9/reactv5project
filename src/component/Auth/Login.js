import Card from 'antd/es/card/Card';
import React from 'react';
import { Button, Form, Input } from 'antd';
import { Link, useNavigate } from 'react-router-dom';
import { auth } from '../../utils/Items';

const Login = () => {
  const navigate = useNavigate();

  const onFinish = (values) => {
    const data = auth?.[0];
    console.log(data);
    console.log("values", values);

    if (values.user_name === data.type) {
      // Save user data to local storage
      localStorage.setItem('token', JSON.stringify(data));
      navigate('/');
    } else {
      // Create admin data with user_name and type
      const adminData = auth?.map((item) => {
        return { ...item, name: values.user_name, type: 'admin' };
      });

      // Save admin data to local storage
      localStorage.setItem('token', JSON.stringify(adminData[0]));

      navigate('/admin');
    }
  };

  return (
    <div style={{ width: '300px', height: 'auto', margin: 'auto', justifyContent: 'center' }}>
      <Card>
        <Form layout='vertical' onFinish={onFinish}>
          <Form.Item label="Username" name={'user_name'}>
            <Input />
          </Form.Item>

          <Form.Item label="Password" name={"password"}>
            <Input.Password />
          </Form.Item>

          <Form.Item>
            <div>
              <Button htmlType='submit'>
                Login
              </Button>
              <div>
                <Link to="/auth/signup">Don't have an account? Sign up</Link>
              </div>
            </div>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};

export default Login;
