import { Route, createBrowserRouter, createRoutesFromElements } from "react-router-dom";
import AdminLayout from "../layout/AdminLayout";
import AdminDashBoard from "../component/admin/DashBoard";
import UserLayout from "../layout/UserLayout";
import UserDashBoard from "../component/user/DashBoard";
import AboutUS from "../component/aboutUs/AboutUS";
import PageNotFound from "../component/PageNotFound";
import UserDetails from "../component/user/UserDetails";
import AuthLayout from "../layout/AuthLayout";
import Login from "../component/Auth/Login";
import SignUp from "../component/Auth/SignUP";
import Hotproducts from "../component/user/Hotproducts";
import TrendingVendors from "../component/user/TrendingVendors";


export const Routers = createBrowserRouter(
    createRoutesFromElements(
      <Route>
        <Route path="/" element={<UserLayout/>}>
            <Route index element={<UserDashBoard/>}/>
            <Route path="aboutus" element={<AboutUS/>}></Route>
            <Route path="userdetail/:id" element={<UserDetails/>}></Route>  
            <Route path="/hotproducts" element={<Hotproducts/>}></Route>  
            <Route path="/latestproducts" element={<TrendingVendors/>}></Route>  
        </Route>
        <Route path="/admin" element={<AdminLayout/>}>
            <Route index element={<AdminDashBoard/>}/>
        </Route>
        <Route path="auth" element={<AuthLayout/>}>
          <Route path="login" element={<Login/>}/>
          <Route path="signup" element={<SignUp/>}/>
        </Route>
        
        <Route path="*" element={<PageNotFound/>}/>
       
        
       
      </Route>
    )
  );
  