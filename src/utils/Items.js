export const HeaderItem = [
  {
    name: "About US",
    link: "/aboutus",
  },
  {
    name: "Contact US",
    link: "/contactus",
  },
  {
    name: "Hot products",
    link: "/hotproducts",
  },
  {
    name: "Latest products",
    link: "/latestproducts",
  },
];
export const Auth = [
  {
    name: "Log in",
    link: "auth/login",
  },
  {
    name: "Sign up",
    link: "auth/signup",
  },
];

export const HotProductData = [
  {
    name: "Chelsea Home jersey",
    price: "100",
    brand: "nike",
    image:
      "https://www.superbuy.com.ng/wp-content/uploads/2021/06/Chelsea-Home-Soccer-Jersey-20232024-6.jpg",
    id: 1,
    id: 2,
    rate: 4,
    description:
      "Jersey is a soft stretchy, knit fabric that was originally made from wool.",
    stockItem:45,
    view:145
  },
  {
    name: "Chelsea third jersey",
    price: "200",
    brand: "nike",
    image:
      "https://www.superbuy.com.ng/wp-content/uploads/2020/10/Chelsea-Third-Jersey-20232024-.jpg",
    id: 2,
    rate: 4,
    description:
      "Jersey is a soft stretchy, knit fabric that was originally made from wool.",
    stockItem:45,
    view:129  
  },
  {
    name: "Chelsea away jersey",
    price: "300",
    brand: "zara",
    image:
      "https://www.superbuy.com.ng/wp-content/uploads/2021/08/Chelsea-Away-Soccer-Jersey-20232024.jpg",
    id: 3,
    rate: 5,
    description:
      "Jersey is a soft stretchy, knit fabric that was originally made from wool.",
    stockItem:46,
    view:129  
  },
  {
    name: "chelsea home jersey 2022",
    price: "400",
    brand: "jordan",
    image:
      "https://www.superbuy.com.ng/wp-content/uploads/2021/06/Chelsea-Home-Womens-Football-Shirt-21-22.jpg",
    id: 4,
    rate: 4,
    description:
      "Jersey is a soft stretchy, knit fabric that was originally made from wool.",
    stockItem:5,
    view:130 
  },
  {
    name: "chelsea home jersey 2022",
    price: "400",
    brand: "jordan",
    image:
      "https://images.footballfanatics.com/chelsea/chelsea-nike-goalkeeper-stadium-shirt-2023-24_ss5_p-13387595+pv-1+u-zsrxdrt0xrxf7oa6zz0m+v-c5hwudvuguvdplcmwbyj.jpg?_hv=2&w=900",
    id: 4,
    rate: 4,
    description:
      "Jersey is a soft stretchy, knit fabric that was originally made from wool.",
    stockItem:5,
    view:130 
  },
];
export const TrendingVendorData = [
  {
    name: "T-shirt",
    price: "500",
    brand: "Gucci",
    image:
      "https://images.footballfanatics.com/chelsea/chelsea-cup-home-stadium-sponsored-goalkeeper-shirt-2023-24-kids-with-hampton-24-printing_ss5_p-201028128+u-sw2gufyylyalnjqxym0g+v-uvnogjji2ayporug4pg3.jpg?_hv=2&w=340",
    id: 1,
  },
  {
    name: "bag",
    price: "600",
    brand: "Gucci",
    image:
      "https://images.footballfanatics.com/chelsea/chelsea-nike-strike-track-pants-black_ss5_p-13387529+u-dpdwdkpywwl2hogtxcp4+v-cv9eumja8txmrethcsdr.jpg?_hv=2&w=340",
    id: 2,
  },
  {
    name: "jacket",
    price: "700",
    brand: "zara",
    image:
      "https://images.footballfanatics.com/chelsea/chelsea-nike-strike-hoodied-tracksuit-white-kids_ss5_p-13387582+u-5dgn3t1yzsg9smceclhv+v-hkjzybnlz5wqqoeulmut.jpg?_hv=2&w=340",
    id: 3,
  },
  {
    name: "shoes",
    price: "800",
    brand: "jordan",
    image:
      "https://images.footballfanatics.com/chelsea/chelsea-nike-academy-pro-pre-match-top-blue-womens_ss5_p-13387533+u-kvo5p6b83vhjtcmr6n7f+v-opnv2fvko2ijtfqgvtlx.jpg?_hv=2&w=340",
    id: 4,
  },
];
export const auth=[
  {
    name:"jems",
    contact:"99999999",
    email:"abc@gmail.com",
    id:1,
    token:"Ri1QWE1LQaWEMOEmZ8Z89WQCqeD3Ir4G10RwdjQwmtZBg7xWth7la2A5RpvOwYcs",
    type:"user"
    }
]